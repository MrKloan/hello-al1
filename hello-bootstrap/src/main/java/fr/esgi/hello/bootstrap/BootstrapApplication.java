package fr.esgi.hello.bootstrap;

import org.update4j.Configuration;
import org.update4j.FileMetadata;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.lang.String.format;

public class BootstrapApplication {

    public static void main(final String[] args) throws IOException {
        final var applicationVersion = args[0];
        final var ciJobId = System.getenv("CI_JOB_ID");

        final var configuration = Configuration.builder()
                .baseUri(format("https://gitlab.com/MrKloan/hello-al1/-/jobs/%s/artifacts/raw/", ciJobId))
                // System.getProperty("user.home") // Récupération de la propriété user.home de la JVM, le placeholder est remplacé par update4j
                .basePath("${user.home}/.hello-al1/")
                .property("default.launcher.main.class", "fr.esgi.hello.application.HelloWorld")
                .file(FileMetadata
                        .readFrom(format("hello-application/target/hello-application-%s.jar", applicationVersion))
                        .path("hello.jar")
                        .classpath()
                )
                .build();

        Files.writeString(Path.of("hello-bootstrap/target/hello.xml"), configuration.toString());
    }
}
